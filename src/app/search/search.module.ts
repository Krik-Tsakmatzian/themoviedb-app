import { NgModule } from '@angular/core';
import { SharedModule } from './../core/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';

import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { SearchValidatorDirective } from './search-validator.directive';
import { MovieCollectiosSelectorComponent } from './movie-collections-selector/movie-collections-selector.component';

@NgModule({
  declarations: [
    SearchComponent,
    MovieCollectiosSelectorComponent,
    SearchValidatorDirective
  ],
  imports: [
    SharedModule,
    SearchRoutingModule,
    NgxPaginationModule
  ],
  entryComponents: [
    MovieCollectiosSelectorComponent
  ]
})
export class SearchModule { }
