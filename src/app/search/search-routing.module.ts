﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// COMPONENTS
import { SearchComponent } from './search.component';

const SearchRoutes: Routes = [
    {
        path: '',
        component: SearchComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SearchRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class SearchRoutingModule { }
