// FRAMEWORK
import {
    Component, Input, Output,
    ViewChild, ElementRef, EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

// RXJS
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

// COMPONENTS
import { MovieCollectiosSelectorComponent } from './movie-collections-selector/movie-collections-selector.component';

// SERVICES
import { SearchService } from './search.service';
import { Config } from '../config/config';
import { StoreManagerService } from 'src/app/core/store-manager/store-manager.service';

// TYPES
import { SearchType, SearchResults, SearchResponse } from './search.types';
import { Pager } from '../core/pager';
import { MoviesCollection } from '../movies-collections/movies-collection.types';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less'],
  providers: [ SearchService ]
})
export class SearchComponent {
    @ViewChild('term', { static: true }) public fileInput: ElementRef;
    @Input() public placeholder: string;
    @Input() public name: string;
    @Input() public disabled: boolean;
    @Output() public selectedItem = new EventEmitter<SearchType>();
    public loading = false;
    public message: string;
    public inputSelected: any;
    public imageBaseUrl: string;
    public request: SearchType = {} as SearchType;
    public result: any;
    public searchTermStream: Subject<string> = new Subject();
    public pager: Pager = new Pager();
    public searchSubscribe: Subscription;
    public items: SearchResults[];

    public myStream = this.searchTermStream
        .pipe(
            debounceTime(500),
            distinctUntilChanged()
        )
        .subscribe((term) => {
            // use this to empty result set in case of change
            if (term === null) {
                this.loading = false;
                this.items = [];
            } else {
                this.loading = false;
                this.get(term);
            }
        });

    constructor(
        public dialog: MatDialog,
        private service: SearchService,
        private config: Config,
        private router: Router,
        private store: StoreManagerService) {
        this.imageBaseUrl = this.config.get('imageUrl');
        this.placeholder = 'Find your movie...';
    }


    // Method that pass
    public search(term: any) {
        this.searchTermStream.next(term && term.length > 2 ? term : null);
    }

    // Clear input from search inputbox (reset function)
    public clearInput(e?: Event) {
        // do not bubble event otherwise focus is on the textbox
        if (e) {
            e.stopPropagation();
        }

        // Clear selection
        this.inputSelected = null;
        this.searchTermStream.next(null);
        // Dispatch focus out event, by clicking anywhere else in the document
        this.fileInput.nativeElement.focus();
    }

    // Redirect us to selected movie's details
    public showInfo(id: number) {
        this.router.navigate(['/movie-details' , id]);
    }

    // Pass the new page number and send again the same input
    public pageChanged(event: number) {
        this.pager.page = event;
        this.get(this.inputSelected);
    }

    // Press the add button in movie item on the right
    public addToCollection(item: SearchResults) {
        const dialogRef = this.dialog.open(MovieCollectiosSelectorComponent);

        // When dialog closes grab the selected id and pass the movie to that collection
        dialogRef.afterClosed().subscribe((res) => {

            const collections: MoviesCollection[] = this.store.getOption('movie_collections', 'local');
            // If collections is null or nothing has been selected then exit the return
            if (!collections || !res) {
                return;
            }

            // Find the selected collection depend on id(res) that we grab from diaolog
            const selectedCollection = collections.find((col) => col.id === res);
            if (!selectedCollection.movieList) {
                selectedCollection.movieList = [item];
            } else {
                selectedCollection.movieList.push(item);
            }
            // Save the update movie_collections
            this.store.saveOption('movie_collections', [...collections], 'local');
        });
    }

    private get(term: string) {
        // Means that another get function is still running
        if (this.loading) {
            return;
        }

        this.message = '';
        this.loading = true;

        this.request.page = this.pager.page;
        this.request.query = term;

        // first stop currently executing requests
        if (this.searchSubscribe) {
            this.searchSubscribe.unsubscribe();
        }

        // then start the new request
        this.searchSubscribe = this.service
            .search(this.request)
            .subscribe((response: SearchResponse) => {
                this.loading = false;
                this.items = response && response.results
                    ? response.results
                    : [];

                if (!this.items || this.items.length === 0) {
                    this.message = 'There are no movies with this name';
                    return;
                }

                // Get the paging info
                this.pager.page = response.page;
                this.pager.total_pages = response.total_pages;
                this.pager.total_results = response.total_results;
            },
            (err) => {
                this.loading = false;
                this.items = [] as SearchResults[];
                this.message = 'data.error';
            });
    }

}
