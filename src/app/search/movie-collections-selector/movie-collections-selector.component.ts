
// FRAMEWORK
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// SERVICES
import { StoreManagerService } from 'src/app/core/store-manager/store-manager.service';

import { MoviesCollection } from './../../movies-collections/movies-collection.types';

@Component({
    selector: 'app-movie-collections-selector',
    templateUrl: 'movie-collections-selector.component.html',
    styleUrls: ['./movie-collections-selector.component.less'],
})

export class MovieCollectiosSelectorComponent implements OnInit {
    public collections: MoviesCollection;
    constructor(
        private store: StoreManagerService,
        public dialogRef: MatDialogRef<MovieCollectiosSelectorComponent>) {
    }

    public ngOnInit() {
        /** retrieve movie collections from localstorage */
        this.collections = this.store.getOption('movie_collections', 'local');
    }

    /** Close opened dialog and pass the id of the selected collection */
    public addToColleciton(id: string) {
        this.dialogRef.close(id);
    }
 }
