import { Directive, ElementRef, HostListener, Input } from '@angular/core';


@Directive({
    selector: '[appSearchValidator]'
})

export class SearchValidatorDirective {
    constructor(private el: ElementRef) {}

    @HostListener('input', ['$event']) onInputChange(event: Event) {
        const myValue = this.el.nativeElement.value;

        this.el.nativeElement.value = myValue.replace(/[^a-zA-Z0-9 ]*/g, '');
        if (myValue !== this.el.nativeElement.value) {
            event.stopPropagation();
        }
    }
}
