// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../config/config';

// TYPES
import { SearchType, SearchResults, SearchResponse } from './search.types';

@Injectable()
export class SearchService {

    constructor(
        private http: HttpClient,
        private config: Config) { }

    /** Search function depends on word that is written in search input box */
    public search(obj: SearchType): Observable<SearchResponse> {
        // Return only if term.lengh is more than 2 letter
        if (obj.query.length < 3) {
            return;
        }
        const params = new HttpParams({
            fromObject: {
                api_key: this.config.get('apiKey'),
                query: obj.query,
                page: obj.page ? obj.page.toString() : '1'
            }
        });

        return this.http
            .get(this.config.get('apiUrl') + '/search/movie', { params }) as Observable<SearchResponse>;
    }

}
