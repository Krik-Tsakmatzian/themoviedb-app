import { PagerRequest, PagerResults } from '../core/pager';

export interface SearchType extends PagerRequest {
    query: string;
}

export interface SearchResults {
    poster_path?: string;
    adult?: boolean;
    overview?: string;
    release_date?: string;
    genre_id?: number[];
    id?: number;
    original_title?: string;
    original_language?: string;
    title?: string;
    backdrop_path?: string;
    popularity?: number;
    vote_count?: number;
    video?: boolean;
    vote_average?: number;

}

export interface SearchResponse extends PagerResults {
    results: SearchResults[];
}