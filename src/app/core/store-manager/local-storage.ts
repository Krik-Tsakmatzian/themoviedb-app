export class LocalStore {
    constructor() { }
    /**
     * Inserts or update an item to the store
     * @param  key       The key or identifier of the item
     * @param  value     Contents of the item
     * @param  type      session or local storage
     */

    public set(key: string, value: any, type: 'session' | 'local'): void {
        // Convert object values to JSON
        if (typeof value === 'object' ) {
            value = JSON.stringify(value);
        }

        if (type === 'session') {
            sessionStorage.setItem(key, value);
        } else {
            localStorage.setItem(key, value);
        }
    }

    /**
     * Removes an item from the store with the specified key
     * @param  key  // The key or identifier of the item in store
     * @param  type // Session or local storage
     */

    public remove(key: string, type: 'session' | 'local'): void {
        if (type === 'session') {
            sessionStorage.removeItem(key);
        } else {
            localStorage.removeItem(key);
        }
    }

    /**
     * Removes alls items from the store
     */

    public empty(type: 'session' | 'local'): void {
        if (type === 'session') {
            sessionStorage.clear();
        } else {
            localStorage.clear();
        }
    }

    /**
     * Retrieves an item of the store with the specified key
     * @param  key  // The key or identifier of the item in store
     * @param  type // Session or local storage
     */

    public get(key: string, type: 'session' | 'local'): any {
        let data: any = type === 'session'
                ? sessionStorage.getItem(key)
                : localStorage.getItem(key);
        // Try to parse JSON...
        try {
            data = JSON.parse(data);
        } catch (e) {
            data = data;
        }

        return data;
    }

}
