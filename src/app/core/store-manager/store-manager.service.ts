﻿// FRAMEWORK
import { Injectable } from '@angular/core';

// TYPES
import { LocalStore } from './local-storage';

@Injectable({
     providedIn: 'root'
})
export class StoreManagerService {
     private storeManager: LocalStore;

     constructor() {
          this.storeManager = new LocalStore();
     }

     public getOption(item: string, type: 'session' | 'local') {
          return this.storeManager.get(item, type);
     }

     public saveOption(item: string, data: any,  type: 'session' | 'local') {
          return this.storeManager.set(item, data, type);
     }

     public deleteOption(item: string,  type: 'session' | 'local') {
          return this.storeManager.remove(item, type);
     }

     public clear(type: 'session' | 'local') {
          return this.storeManager.empty(type);
     }
}
