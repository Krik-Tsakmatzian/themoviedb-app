export interface PagerResults {
    page: number;
    total_pages: number;
    total_results: number;
}

export interface PagerRequest {
    page: number;
}


/** Used to represent paging data request. */
export class Pager implements PagerResults {
    /** The index of the requested page */
    public page: number;
    /** The total number of pages */
    public total_pages: number;
    /** The total number of records */
    public total_results: number;

    constructor() {
        this.page = 1;
        this.total_pages = 1;
        this.total_results = 0;
    }

    /** increase the page number by 1 */
    public addPage() {
        this.page += 1;
    }

    /** decrease the page number by 1 */
    public removePage() {
        this.page -= 1;
    }
}

