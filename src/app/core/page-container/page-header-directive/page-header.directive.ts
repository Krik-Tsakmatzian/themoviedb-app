import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appPageHeader]'
})

export class PageHeaderDirective implements AfterViewInit {

    constructor(private elementRef: ElementRef, private renderer: Renderer2) {
       // foo
    }

    public ngAfterViewInit() {
        const cnt = this.elementRef.nativeElement;
        this.renderer.setStyle(cnt, 'display', 'none');
        const that = this;
        setTimeout(() => {
            const pl = document.getElementById('title-placeholder');
            if (pl) {
                pl.innerHTML = '';
                that.renderer.setStyle(cnt, 'display', 'block');
                pl.appendChild(cnt);
            }
        }, 5);
     }
}
