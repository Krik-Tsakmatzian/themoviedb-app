// FRAMEWORK
import {
    Component, ViewEncapsulation, Input,  ViewChild,
    ElementRef, Output, EventEmitter, AfterViewInit
} from '@angular/core';


@Component({
    selector: 'app-page-container',
    templateUrl: 'page-container.component.html',
    styleUrls: ['page-container.component.less']
})

export class PageContainerComponent implements AfterViewInit {
    @ViewChild('pageContent', { static: false }) public pageContents: ElementRef;
    @Input() public title: string;
    @Input() public backButton: boolean;

    @Output() public notifyOnBack: EventEmitter<any> = new EventEmitter();

    constructor() {}

    public ngAfterViewInit(): void {
    }

    // Back to page
    public onBack(): void {
        this.notifyOnBack.emit();
    }


}
