// FRAMEWORK
import { NgModule } from '@angular/core';
import { SharedModule } from './../core/shared.module';

import { MoviesCollectionsRoutingModule } from './movies-collections-routing.module';

// COMPONENTS
import { MoviesCollectionsComponent } from './movies-collections.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { EditMoviesCollectionComponent } from './edit/edit-movies-collection.component';

@NgModule({
    declarations: [
        MoviesCollectionsComponent,
        EditMoviesCollectionComponent,
        MoviesListComponent
    ],
    imports: [
        SharedModule ,
        MoviesCollectionsRoutingModule
    ]
})
export class MoviesCollectionsModule { }
