export interface MoviesCollection {
    id: string;
    title: string;
    description: string;
    movieList: any[];
}
