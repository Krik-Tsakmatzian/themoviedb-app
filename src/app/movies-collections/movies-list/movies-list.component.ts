// FRAMEWORK
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// SERVICES
import { StoreManagerService } from './../../core/store-manager/store-manager.service';
import { Config } from '../../config/config';

// TYPES
import { MoviesCollection } from '../movies-collection.types';
import { Movie } from './movies-list.types';


@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.less']
})
export class MoviesListComponent implements OnInit {
    public collection: MoviesCollection = {} as MoviesCollection;
    public selectListId: string;
    public movies: Movie[];
    public imageBaseUrl: string;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private config: Config,
        private store: StoreManagerService
    ) {
        this.imageBaseUrl = this.config.get('imageUrl');
    }

    ngOnInit() {
        /** Grab from URL tha params. If there is an id then we call get function
         */
        this.activatedRoute
            .params
            .subscribe((params: {
                id: string;
            }) => {
                if (params.id) {
                    this.selectListId = params.id;
                    this.get(this.selectListId);
                }
            });
    }

    /** Back to previous page */
    public back() {
        this.router.navigate(['/movies-collections']);
    }

    /** find collection from localstorage */
    public get(id: string) {
        const myCollections = this.store.getOption('movie_collections', 'local');
        this.collection = myCollections.find((col) => col.id === id);
        /** If it is null, redirect us to movies-collection url */
        if (!this.collection) {
            this.router.navigate(['/movies-collections']);
        }

        this.movies = this.collection.movieList;
    }

    /** Remove movie from our Movies collection list */
    public remove(id: number) {

        const myCollections = this.store.getOption('movie_collections', 'local');
        if (!this.collection) {
            this.router.navigate(['/movies-collections']);
        }

        this.movies = this.collection.movieList.filter((movie) => {
            return movie.id !== id;
        });

        myCollections.forEach((collection) => {
            if (collection.id === this.selectListId) {
                collection.movieList = [...this.movies];
            }
        });

        this.store.saveOption('movie_collections', myCollections, 'local');
    }

    /** Navigate us to movie details page for the specific movie */
    public showInfo(id: number) {
        this.router.navigate(['/movie-details' , id]);
    }

}
