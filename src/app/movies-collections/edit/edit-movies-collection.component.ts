
// FRAMEWORK
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// SERVICES
import { StoreManagerService } from './../../core/store-manager/store-manager.service';

// MODELS
import { MoviesCollection } from '../movies-collection.types';


@Component({
  selector: 'app-edit-movies-collection',
  templateUrl: './edit-movies-collection.component.html',
  styleUrls: ['./edit-movies-collection.component.less']
})
export class EditMoviesCollectionComponent implements OnInit {
    @ViewChild('editForm', { static: true }) public editForm: NgForm;
    public action: string;
    public collection: MoviesCollection = {} as MoviesCollection;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private store: StoreManagerService) { }

    ngOnInit() {
        /** Grab from URL tha params. If there is an id then we edit an existing item
         *  so we must get this item. Else we have to create a new one
         */
        this.activatedRoute
            .params
            .subscribe((params: {
                editMode: string;
                id: string;
            }) => {
                this.action = params.id
                            ? 'Edit '
                            : 'Create ';
                if (params.id) {
                    this.get(params.id);
                }
            });
    }

    /** Back to previous page */
    public back() {
        this.router.navigate(['/movies-collections']);
    }

    /** Create a random uuid for our movie collections */
    public uuid() {
        const s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        };
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    /** Submit form */
    public onSubmit(val) {
        /**  truck my collection (new or old) */
        const myCollection: MoviesCollection = {
            id: !this.collection.id ? this.uuid() : this.collection.id,
            title: val.form.value.title,
            description: val.form.value.description,
            movieList: this.collection.movieList
        };

        /** Get from localstore all movie Collections */
        let collections: MoviesCollection[] = this.store.getOption('movie_collections', 'local');

        /** collections is null then save my trucked collection, else update the trucked collection */
        if (!collections) {
            this.store.saveOption('movie_collections', [myCollection], 'local');
        } else {
            if (this.collection.id) {
                collections = collections.filter((col) => {
                    return col.id !== this.collection.id;
                });
            }
            collections.push(myCollection);
            this.store.saveOption('movie_collections', [...collections], 'local');
        }

        /** finally navigate us back in movie-collections */
        this.router.navigate(['/movies-collections']);

    }

    /** find collection from localstorage */
    public get(id: string) {
        const myCollections = this.store.getOption('movie_collections', 'local');
        this.collection = myCollections.find((col) => col.id === id);
        if (!this.collection) {
            /** If null navigate us back to create new collection */
            this.router.navigate(['/movies-collections' , 'add']);
        }
    }

}
