import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// SERVICES
import { StoreManagerService } from './../core/store-manager/store-manager.service';

// MODELS
import { MoviesCollection } from './movies-collection.types';


@Component({
  selector: 'app-movies-collections',
  templateUrl: './movies-collections.component.html',
  styleUrls: ['./movies-collections.component.less']
})
export class MoviesCollectionsComponent implements OnInit {
  public cards: MoviesCollection[];
  constructor(
        private router: Router,
        private store: StoreManagerService) { }

  ngOnInit() {
    /** get movie collections from localstorage */
    this.cards = this.store.getOption('movie_collections', 'local');
  }

  /** add new collection we redirect us in add page */
  public addNew() {
    this.router.navigate(['/movies-collections', 'add']);
  }

  /** redirect us to the movies list of a specific collection */
  public movieList(id: string) {
    this.router.navigate(['/movies-collections', 'list', id]);
  }

}
