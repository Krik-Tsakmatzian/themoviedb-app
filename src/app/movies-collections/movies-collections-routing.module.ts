﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesCollectionsComponent } from './movies-collections.component';
import { EditMoviesCollectionComponent } from './edit/edit-movies-collection.component';
import { MoviesListComponent } from './movies-list/movies-list.component';

const MoviesCollectionsRoutes: Routes = [
    {
        path: '',
        component: MoviesCollectionsComponent
    },
    {
        path: 'add',
        component: EditMoviesCollectionComponent
    },
    {
        path: 'edit/:id',
        component: EditMoviesCollectionComponent
    },
    {
        path: 'list/:id',
        component: MoviesListComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MoviesCollectionsRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class MoviesCollectionsRoutingModule { }
