import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './core/404error/page-not-found.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/search',
        pathMatch: 'full'
    },
    {
        path: 'search',
        loadChildren: () => import('./search/search.module').then((m) => m.SearchModule)
    },
    {
        path: 'movies-collections',
        loadChildren: () => import('./movies-collections/movies-collections.module').then((m) => m.MoviesCollectionsModule)
    },
    {
        path: 'movie-details',
        loadChildren: () => import('./movie-details/movie-details.module').then((m) => m.MovieDetailsModule)
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
