// FRAMEWORK
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

// COMPONENTS
import { MovieDialogDetailsComponent } from './movie-dialog-details/movie-dialog-details.component';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html'
})

export class MovieDetailsComponent implements OnInit {
    public movie: any;
    constructor(
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location) { }

    ngOnInit() {
        /** Get data from resolver */
        this.movie = this.route.snapshot.data;

        /** Open dialog and pass data to it */
        const dialogRef = this.dialog.open(MovieDialogDetailsComponent, {
            data: this.movie
        });

        /** When dialog closes, thes redirect us back to the previous url */
        dialogRef.afterClosed().subscribe((res) => {
            this.location.back();
        });
    }

}

