// FRAMEWORK
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

// RXJS
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

// SERVICES
import { MovieDetailsService } from './movie-details.service';

// TYPES
import { MovieDetails } from './movie-details.types';

@Injectable()
export class MovieDetailsResolver implements Resolve<Observable<MovieDetails | string>> {
    constructor(private movieDetailsService: MovieDetailsService) {}

    /** Get movie before start loading component */
    public resolve(route: ActivatedRouteSnapshot) {
        return this.movieDetailsService
            .getMovie(route.paramMap.get('movie_id'))
            .pipe(
                catchError(() => {
                    return of('data not available at this time');
                })
            );
    }
}
