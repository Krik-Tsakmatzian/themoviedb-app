import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared.module';

import { MovieDetailsComponent } from './movie-details.component';
import { MovieDetailsRoutingModule } from './movie-details-routing.module';
import { MovieDialogDetailsComponent } from './movie-dialog-details/movie-dialog-details.component';
import { MovieDetailsResolver } from './movie-details.resolver';
import { MovieDetailsService } from './movie-details.service';

@NgModule({
    declarations: [
        MovieDetailsComponent,
        MovieDialogDetailsComponent
    ],
    imports: [
        MovieDetailsRoutingModule,
        SharedModule
    ],
    entryComponents: [
        MovieDialogDetailsComponent
    ],
    providers: [
        MovieDetailsService,
        MovieDetailsResolver
    ]
})
export class MovieDetailsModule { }
