// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from '../config/config';
import { StoreManagerService } from '../core/store-manager/store-manager.service';

// TYPES
import { MovieDetails, RateResponse } from './movie-details.types';

@Injectable()
export class MovieDetailsService {

    constructor(
        private http: HttpClient,
        private store: StoreManagerService,
        private config: Config) { }

    /** Get movie from API searching by ID */
    public getMovie(id: string): Observable<MovieDetails> {
        const params = new HttpParams({
            fromObject: {
                api_key: this.config.get('apiKey')
            }
        });

        return this.http
            .get(this.config.get('apiUrl') + '/movie/' + id, { params }) as Observable<MovieDetails>;
    }

    /** Post a movie rate by passing movieId and rate number */
    public rateMovie(movieId: string, rate: number): Observable<RateResponse> {
        const params = new HttpParams({
            fromObject: {
                api_key: this.config.get('apiKey'),
                guest_session_id: this.store.getOption('guestSession', 'session').guest_session_id
            }
        });

        return this.http
                .post(
                    this.config.get('apiUrl') + '/movie/' + movieId + '/rating?' + params.toString(),
                    { value: rate }
                ) as Observable<RateResponse>;
    }
}
