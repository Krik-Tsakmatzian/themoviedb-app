﻿// FRAMEWORK
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// COMPONENTS
import { MovieDetailsComponent } from './movie-details.component';

// RESOLVERS
import { MovieDetailsResolver } from './movie-details.resolver';

const MovieDetailsRoutes: Routes = [
    {
        path: ':movie_id',
        component: MovieDetailsComponent,
        resolve: {
            movie: MovieDetailsResolver
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MovieDetailsRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        MovieDetailsResolver
    ]
})

export class MovieDetailsRoutingModule { }
