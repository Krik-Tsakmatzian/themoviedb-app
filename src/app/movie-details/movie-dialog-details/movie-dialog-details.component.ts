// FRAMEWORK
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

// SERVICES
import { Config } from '../../config/config';
import { MovieDetailsService } from '../movie-details.service';

@Component({
    selector: 'app-movie-dialog-details',
    templateUrl: 'movie-dialog-details.component.html',
    styleUrls: ['./movie-dialog-details.component.less'],
    providers: [ MovieDetailsService ]
})

export class MovieDialogDetailsComponent {
    public imageBaseUrl: string;
    public rateValue: number;
    public showRate = true;
    constructor(
        private config: Config,
        private service: MovieDetailsService,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.imageBaseUrl = this.config.get('imageUrl');
    }

    /** Submit your rate to a movie */
    public submitRate() {
        this.service.rateMovie(this.data.movie.id.toString(), this.rateValue)
            .subscribe((res) => {
                if (res && (res.status_code !== 7 && res.status_code !== 34 ) ) {
                    this.showRate = false;
                }
            });
    }
 }
