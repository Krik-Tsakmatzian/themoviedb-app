// FRAMEWORK
import { Component, ViewChild, HostListener, OnInit, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatSidenav } from '@angular/material/sidenav';

// RXJS
import { Subscription } from 'rxjs';

// SERVICES
import { AppService } from './app.service';
import { StoreManagerService } from './core/store-manager/store-manager.service';

// TYPES
import { SideBar } from './core/sidebar.types';
import { GuestSession } from './app.types';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.less',
        './core/styles/sidebar.less',
        './core/styles/toolbar.less'
    ],
    animations: [
        trigger(
            'openClose',
            [
                state('collapsed', style({ display: 'none', opacity: '0', height: '0px' })),
                state('expanded', style({ display: 'block', opacity: '1', height: '*' })),
                transition('collapsed <=> expanded', animate('500ms ease-out'))
            ]
        )
    ]
})
export class AppComponent implements OnInit, OnDestroy {
    @ViewChild('start', { static: false }) public sidenav: MatSidenav;
    public mode = 'side';
    public active: boolean;
    public menu = true;
    public opening = true;
    public guestSessionSubsciption: Subscription;
    public sidebarOptions: SideBar[] = [
        {
            title: 'Search a movie',
            routeParam: 'search',
        },
        {
            title: 'My movies collections',
            routeParam: 'movies-collections',
        }
    ];
    public title = 'The Movie DB';

    constructor(
        private service: AppService,
        private storeManager: StoreManagerService) {}

    public ngOnInit() {
        if (!this.storeManager.getOption('guestSession', 'session')) {
            this.guestSessionSubsciption = this.service.getGuestSessionId().subscribe((res: GuestSession) => {
                if (res.success) {
                    this.storeManager.saveOption('guestSession', res, 'session');
                }
            });
        }

    }

     public ngOnDestroy() {
         if (this.guestSessionSubsciption) {
             this.guestSessionSubsciption.unsubscribe();
         }
     }

    // checks if starting window width has changed and if it's
    // less than 880px it hides sidebar (usually this is functional in
    // large mobiles and tablets while rotating the screen.
    @HostListener('window:resize') public onResize() {
        if (window.innerWidth > 880) {
            this.opening = true;
            this.mode = 'side';
        } else {
            this.opening = false;
            this.mode = 'over';
        }
    }
    public openClose() {
        if (this.mode === 'over') {
            setTimeout(() => {
                this.sidenav.toggle();
            }, this.sidenav.opened ? 600 : 0);
        }
    }
}
