// FRAMEWORK
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// SERVICES
import { Config } from './config/config';

// TYPES
import { GuestSession } from './app.types';

@Injectable({
    providedIn: 'root'
})
export class AppService {

    constructor(
        private http: HttpClient,
        private config: Config) { }

    public getGuestSessionId(): Observable<GuestSession> {

        const params = new HttpParams({
            fromObject: {
                api_key: this.config.get('apiKey')
            }
        });

        return this.http
            .get(this.config.get('apiUrl') + '/authentication/guest_session/new', { params }) as Observable<GuestSession>;
    }

}
